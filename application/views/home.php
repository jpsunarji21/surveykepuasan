<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Survey Kepuasan</title>
    <style>
        .card-deck .card {
            min-width: 220px;
        }
    </style>
</head>

<body>
    <div class="container">
        <center style="padding-top: 20px; padding-bottom: 20px">
            <h1>Survey Kepuasan Atas Pelayanan</h1>
        </center>
        <div class="card-deck mb-3 text-center">
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Rekomendasi Izin Pendirian PIHK</h4>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url(); ?>isi_survey/1" class="btn btn-lg btn-block btn-primary">Isi Survey</a>
                </div>
            </div>
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Izin Pengesahan Penyelenggaraan PIHK</h4>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url(); ?>isi_survey/2" class="btn btn-lg btn-block btn-primary">Isi Survey</a>
                </div>
            </div>
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Pelayanan Pendaftaran Haji Khusus</h4>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url(); ?>isi_survey/3" class="btn btn-lg btn-block btn-primary">Isi Survey</a>
                </div>
            </div>
        </div>
        <div class="card-deck mb-3 text-center">
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Pembatalan Berangkat Haji Khusus</h4>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url(); ?>isi_survey/4" class="btn btn-lg btn-block btn-primary">Isi Survey</a>
                </div>
            </div>
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Rekomendasi Ijin Pendirian PPIU</h4>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url(); ?>isi_survey/5" class="btn btn-lg btn-block btn-primary">Isi Survey</a>
                </div>
            </div>
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Izin Pengesahan PPIU</h4>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url(); ?>isi_survey/6" class="btn btn-lg btn-block btn-primary">Isi Survey</a>
                </div>
            </div>
        </div>
        <div class="card-deck mb-3 text-center">
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Rekomendasi Izin Pendirian KBIH</h4>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url(); ?>isi_survey/7" class="btn btn-lg btn-block btn-primary">Isi Survey</a>
                </div>
            </div>
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Pelimpahan No Porsi Jemaah haji wafat & Cacat Permanen</h4>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url(); ?>isi_survey/8" class="btn btn-lg btn-block btn-primary">Isi Survey</a>
                </div>
            </div>
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Izin Penggunaan Asrama Haji Transit</h4>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url(); ?>isi_survey/9" class="btn btn-lg btn-block btn-primary">Isi Survey</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>
<!DOCTYPE html>
<html>

<head>
	<title>Survei Kepuasan</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
	<!-- javascript -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="header">
				<h1>Survei Penilain Kepuasan <br /> <?php echo $kategori ?> </h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<center>
					<?php
					if ($jenis === "isi_survey") {
					?>
						<a href="<?php echo base_url(); ?>" class="btn btn-info">Home</a>
					<?php
					}
					if ($jenis === "hasil_survey") {
					?>
						<a href="<?php echo base_url('hasil'); ?>" class="btn btn-info">Home</a>
					<?php } ?>
					<!-- <a href="<?php echo base_url('web/result'); ?>" class="btn btn-info">Result</a> -->
				</center>
				<br />
			</div>
		</div>

		<!--content is here :)-->
		<?php echo $content; ?>
	</div>
</body>

</html>
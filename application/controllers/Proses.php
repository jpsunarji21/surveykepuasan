<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proses extends CI_Controller
{
	public function AddSurvei($kategori)
	{
		date_default_timezone_set("Asia/Makassar");
		$tgl = date("Y-m-d");
		$data = array(
			"kategori" => $kategori,
			"tanggal" => $tgl,
			"respon" => $_POST['get']
		);
		$this->mod_survei->InsertData('data', $data);
	}
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Web extends CI_Controller
{
	public function index()
	{
		$con = array(
			"kategori" => "Testing",
			"content" => $this->Content()
		);
		$this->load->view('web', $con);
	}

	public function pilihsurvey($kategori)
	{
		switch ($kategori) {
			case "1":
				$nama_kategori = "Rekomendasi Izin Pendirian PIHK";
				break;
			case "2":
				$nama_kategori = "Izin Pengesahan Penyelenggaraan PIHK";
				break;
			case "3":
				$nama_kategori = "Pelayanan Pendaftaran Haji Khusus";
				break;
			case "4":
				$nama_kategori = "Pembatalan Berangkat Haji Khusus";
				break;
			case "5":
				$nama_kategori = "Rekomendasi Ijin Pendirian PPIU";
				break;
			case "6":
				$nama_kategori = "Izin Pengesahan PPIU";
				break;
			case "7":
				$nama_kategori = "Rekomendasi Izin Pendirian KBIH";
				break;
			case "8":
				$nama_kategori = "Pelimpahan No Porsi Jemaah haji wafat & Cacat Permanen";
				break;
			case "9":
				$nama_kategori = "Izin Penggunaan Asrama Haji Transit";
				break;
			default:
				echo "Data tidak ditemukan";
				return;
		}
		$con = array(
			"jenis" => "isi_survey",
			"kategori" => $nama_kategori,
			"content" => $this->Content()
		);
		$this->load->view('web', $con);
	}

	public function Result($kategori)
	{
		switch ($kategori) {
			case "1":
				$nama_kategori = "Rekomendasi Izin Pendirian PIHK";
				break;
			case "2":
				$nama_kategori = "Izin Pengesahan Penyelenggaraan PIHK";
				break;
			case "3":
				$nama_kategori = "Pelayanan Pendaftaran Haji Khusus";
				break;
			case "4":
				$nama_kategori = "Pembatalan Berangkat Haji Khusus";
				break;
			case "5":
				$nama_kategori = "Rekomendasi Ijin Pendirian PPIU";
				break;
			case "6":
				$nama_kategori = "Izin Pengesahan PPIU";
				break;
			case "7":
				$nama_kategori = "Rekomendasi Izin Pendirian KBIH";
				break;
			case "8":
				$nama_kategori = "Pelimpahan No Porsi Jemaah haji wafat & Cacat Permanen";
				break;
			case "9":
				$nama_kategori = "Izin Penggunaan Asrama Haji Transit";
				break;
			default:
				echo "Data tidak ditemukan";
				return;
		}
		$con = array(
			"jenis" => "hasil_survey",
			"kategori" => $nama_kategori,
			"content" => $this->GetResult($kategori)
		);
		$this->load->view('web', $con);
	}
	private function Content()
	{
		return $this->load->view('content', array(), true);
	}
	public function GetResult($kategori)
	{
		$data = $this->mod_survei->GetResult($kategori);
		return $this->load->view('result', $data, true);
	}
}
